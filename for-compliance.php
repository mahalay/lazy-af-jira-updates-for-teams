#!/usr/bin/php
<?php

use Mahalay\LazyAfJIraUpdatesForTeams\Sebbmyr\StatusUpdateCard;
use Sebbmyr\Teams\TeamsConnector;
use Symfony\Component\Yaml\Yaml;
use Unirest\{Request, Response};

require_once __DIR__ . '/vendor/autoload.php';
$configFilePath = \is_readable(__DIR__.'/config.yaml') ? __DIR__.'/config.yaml' : __DIR__.'/config.yaml.dist';

if (!\is_readable($configFilePath)) {
    throw new RuntimeException('Missing config.yaml in root directory, see config.yaml.dist for reference');
}

$config = Yaml::parseFile($configFilePath);
['teams' => $teamsConfig, 'jira' => $jiraConfig] = $config;

$teamsConnector = new TeamsConnector($teamsConfig['webhook-url']);

$itemsDoneLastTimeMessageBody = createStatusUpdateMessage(
    queryJiraIssues(
        $jiraConfig,
        'assignee was currentUser() AND status IN("In Progress", Closed, Done) AND updatedDate >= -4w '
        . 'order by updatedDate DESC'
    ),
    $jiraConfig,
    'What I Did Last Time'
);

$itemsTodoTodayMessageBody = createStatusUpdateMessage(
    queryJiraIssues($jiraConfig, 'assignee = currentUser() AND status IN("In Progress") order by updatedDate DESC'),
    $jiraConfig,
    'What I Will Do Today'
);

$messageCard = new StatusUpdateCard('', $itemsDoneLastTimeMessageBody . $itemsTodoTodayMessageBody);
$teamsConnector->send($messageCard);

function generateWorkItems(array $decodedResponse, array $jiraConfig): string
{
    $listItems = '';
    foreach ($decodedResponse['issues'] as $issue) {

        $reconstructedComment = extractLastComment($issue);

        $listItems .= <<<LIST_ITEM
<li>
    <a href="{$jiraConfig['base_url']}/browse/{$issue['key']}"><strong>[{$issue['key']}]</strong></a>
    <span>{$issue['fields']['summary']}</span><br />
    {$reconstructedComment}
</li>
LIST_ITEM;
    }

    return $listItems;
}

function extractLastComment(array $issue): string
{
    if ($issue['fields']['comment']['comments'] ?? []) {
        $lastComment = \end($issue['fields']['comment']['comments']);

        \ob_start();
        foreach ($lastComment['body']['content'] as $paragraph) {
            if (\count($paragraph['content']) < 1) {
                continue;
            }

            \ob_start();
            foreach ($paragraph['content'] as $phrase) {

                $phraseText = $phrase['text'] ?? $phrase['attrs']['text'] ?? $phrase['type'];
                $phraseFormat = commentPhraseFormatMap()[$phrase['type']] ?? false;
                if (false === $phraseFormat) {
                    continue;
                }

                echo sprintf($phraseFormat, $phraseText);
            }
            $paragraphContent = \ob_get_clean();

            if ($paragraphContent) {
                echo '<p><em>', $paragraphContent, '</em></p>';
            }
        }
        $comments = ob_get_clean();

        $updateDate = new \DateTimeImmutable($lastComment['updated']);
        return <<<COMMENT
<span>Last Comment by </span>
<strong>{$lastComment['author']['displayName']}</strong>
<span>({$updateDate->format('Y-m-d H:i:s')}): </span>
{$comments}
<br />
COMMENT;
    }

    return '';
}

function commentPhraseFormatMap(): array
{
    return [
        'text' => '%s ',
        'mention' => '<strong>%s </strong>',
        'media' => '(attachment:%s) ',
    ];
}

function queryJiraIssues(array $jiraConfig, string $jql): Response
{
    $headers = [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json'
    ];

    $query = [
        'expand' => [
            'names',
            'schema',
            'operations'
        ],
        'jql' => $jql,
        'maxResults' => 5,
        'startAt' => 0,
        'fields' => ['key', 'summary', 'status', 'comment'],
        'fieldsByKeys' => false,
    ];

    Request::auth($jiraConfig['user_id_or_email'], $jiraConfig['token']);
    $response = Request::post("{$jiraConfig['base_url']}/rest/api/3/search", $headers, \json_encode($query));

    return $response;
}

function createStatusUpdateMessage(Response $response, array $jiraConfig, string $messageHeader): string
{
    if (200 === $response->code) {
        $decodedResponse = \json_decode($response->raw_body, true);

        if (\count($decodedResponse['issues'])) {
            $listItems = generateWorkItems($decodedResponse, $jiraConfig);

            return <<<BODY
<strong>{$messageHeader}:</strong><br />
<ul>
    {$listItems}
</ul>
BODY;
        }
    }

    return '';
}
