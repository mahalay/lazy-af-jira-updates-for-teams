<?php

namespace Mahalay\LazyAfJIraUpdatesForTeams\Sebbmyr;

use Sebbmyr\Teams\TeamsConnectorInterface;

class StatusUpdateCard implements TeamsConnectorInterface
{
    /** @var string */
    private $title;

    /** @var string */
    private $body;

    public function __construct(string $title, string $body)
    {
        $this->title = $title;
        $this->body = $body;
    }

    public function getMessage()
    {
        return [
            '@context' => 'http://schema.org/extensions',
            '@type' => 'MessageCard',
            'themeColor' => '0072C6',
            'text' => $this->body
        ] + (($this->title) ? ['title' => $this->title] : []);
    }
}
