# Lazy AF Jira Updates for Teams
A simple PHP script that provides updates in teams (usually a channel). Use this if your organization, for some shitty reason, is unable to utilize Jira but instead would require you to do updates in MS Teams.

## Installation
```
$ composer update -o
```

## Configuration
- copy `config.yaml.dist` to `config.yaml`
- edit `config.yaml`

## How to Use
Simply:
```
$ ./for-compliance.php
